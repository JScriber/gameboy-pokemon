const express = require('express');
const http = require('http');
const path = require('path');

const app = express();

// Serve only the static files form the dist directory.
app.use(express.static(__dirname + '/dist/'));

app.get('*', (_, res) => res.sendFile(path.join(__dirname + '/dist/index.html')));

const PORT = process.env.PORT || 8080;

const server = http.createServer(app);

// Start the app by listening on the default Heroku port
server.listen(PORT, () => console.log("Running on port " + PORT));
