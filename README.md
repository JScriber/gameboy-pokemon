# Quick setup

```bash
# Download the project.
git clone https://gitlab.com/JScriber/gameboy-pokemon.git pokemon

# Enter the folder.
cd !$

# Install dependencies
npm install

# Run the project
npm run dev
```
