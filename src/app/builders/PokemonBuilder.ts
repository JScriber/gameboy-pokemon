import { Builder } from './Builder';
import { Pokemon } from '../models/Pokemon';
import { Skill } from '../models/Skill';
import { Type } from '../models/Type';
import { Attack } from '../models/Attack';
import { Stat } from '../models/Stat';
import { Gender } from '../models/gender';

interface PokemonBuild {
  id: number;
  name: string;
  gender: Gender;

  skill: Skill;
  level: number;
  types: Type[];
  attacks: Attack[];

  hp: number;
  attack: number;
  specialAttack: number;
  shield: number;
  specialShield: number;
  speed: number;

  graphicID: number;
}

export class PokemonBuilder implements Builder<PokemonBuild, Pokemon> {

  private static _instance: PokemonBuilder;

  static get instance() {
    if (!this._instance) this._instance = new this();

    return this._instance;
  }

  private constructor() {}


  // Implementation.
  build(parameters: PokemonBuild): Pokemon {

    const pokemon: Pokemon = (Object as any).assign(new Pokemon(), parameters);

    pokemon.hp = new Stat(parameters.hp);
    pokemon.attack = new Stat(parameters.attack);
    pokemon.specialAttack = new Stat(parameters.specialAttack);
    pokemon.shield = new Stat(parameters.shield);
    pokemon.specialShield = new Stat(parameters.specialShield);
    pokemon.speed = new Stat(parameters.speed);

    return pokemon;
  }

}
