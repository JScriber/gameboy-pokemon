import { Builder } from './Builder';
import { Attack } from '../models/Attack';
import { Type } from '../models/Type';
import { MoveType } from '../models/MoveType';
import { Stat } from '../models/Stat';

interface AttackBuild {
  name: string;
  type: Type;
  moveType: MoveType;
  power: number;
  pp: number;
}

export class AttackBuilder implements Builder<AttackBuild, Attack> {

  private static _instance: AttackBuilder;

  static get instance() {
    if (!this._instance) this._instance = new this();

    return this._instance;
  }

  private constructor() {}

  // Implementation.
  build(parameters: AttackBuild): Attack {

    const attack: Attack = (Object as any).assign(new Attack(), parameters);

    attack.pp = new Stat(parameters.pp);

    return attack;
  }

}
