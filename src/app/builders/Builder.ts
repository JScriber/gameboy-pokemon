export interface Builder<P, E> {
  build(parameters: P): E;
}
