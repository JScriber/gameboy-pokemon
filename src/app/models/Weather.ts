export enum Weather {
  NORMAL,
  SUNSHINE,
  RAIN,
  MIST
}
