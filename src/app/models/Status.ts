export enum Status {
  NONE,
  ASLEEP,
  POISONED,
  BURNT,
  PARALYSED
}
