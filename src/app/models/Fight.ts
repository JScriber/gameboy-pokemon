import { Trainer } from './Trainer';
import { Weather } from './Weather';

export class Fight {

  player1: Trainer;

  player2: Trainer;

  weather: Weather;
}
