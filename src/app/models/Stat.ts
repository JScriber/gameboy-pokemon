export class Stat {

  current: number;

  constructor(public readonly base: number) {
    this.current = base;
  }
}
