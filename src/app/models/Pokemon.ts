import { Stat } from './Stat';
import { Skill } from './Skill';
import { Type } from './Type';
import { Attack } from './Attack';
import { Status } from './Status';
import { Gender } from './gender';

export class Pokemon {

  id: number;

  name: string;

  graphicID: number;

  skill: Skill;

  level: number;

  types: Type[];

  attacks: Attack[];

  hp: Stat;

  attack: Stat;

  specialAttack: Stat;

  shield: Stat;

  specialShield: Stat;

  speed: Stat;

  gender: Gender;

  status: Status = Status.NONE;

  get isKo() {
    return this.hp.current === 0;
  }
}
