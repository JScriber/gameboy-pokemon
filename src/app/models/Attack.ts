import { Stat } from './Stat';
import { MoveType } from './MoveType';
import { Type } from './Type';

export class Attack {

  name: string;

  type: Type;

  moveType: MoveType;

  power: number;

  pp: Stat;

  hasEnoughPP() {
    return this.pp.current > 0;
  }
}
