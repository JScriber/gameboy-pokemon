import { Pokemon } from './Pokemon';

export type Team = Pokemon[];

export class Trainer {

  name: string;

  pokemons: Pokemon[];
  
  bot: boolean;
}
