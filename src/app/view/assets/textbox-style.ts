/**
 * Different textbox style.
 */
export enum TextboxStyle {
  POKEMON_CENTER = 1,
  PLAIN = 2,
  SQUARED = 3
}
