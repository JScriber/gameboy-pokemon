/**
 * Style of the platforms.
 */
export enum PlatformStyle {
  GRASS = 0,
  GROUND = 1,
  DAMP = 2,
  MUDDY = 3,
  DRY = 4,
  ROCK = 5,
  SAND = 6,
  WATER = 7,
  SNOW = 8,
  CAVE = 9,
  ARENA_1 = 10,
  ARENA_2 = 11,
  ARENA_3 = 12,
  PLAIN = 13,
  WHITE = 14
}
