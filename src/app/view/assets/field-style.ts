/**
 * Style of the field.
 */
export enum FieldStyle {
  FOREST = 0,
  MOUNTAIN = 1,
  SEA = 2,
  SNOW = 3,
  CAVE = 4,
  DESERT = 5,
  VALLEY = 6,
}
