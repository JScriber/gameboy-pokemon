import { Direction } from '../../shapes/menu/menu-shape';

/**
 * A controllable element provides some navigation control and a way to access the selected data.
 * @template T - Type of selected value.
 */
export interface Controllable<T> {

  /**
   * Element currently selected.
   */
  selectedValue: T;

  /**
   * Navigates to the given direction.
   * @param direction
   */
  navigate(direction: Direction);

}
