import Konva from 'konva';
import { loadImage } from '../../utils/load-image';
import { TextboxGroup } from './textbox-group';
import { Action } from '../../../view/view';
import { MenuShape, Direction } from '../../shapes/menu/menu-shape';

// Behaviours.
import { Controllable } from './controllable.model';

export class ActionGroup extends Konva.Group implements Controllable<Action> {

  public static readonly WIDTH = 140;

  private menu = new MenuShape<Action>({
    width: ActionGroup.WIDTH + 10,
    height: TextboxGroup.HEIGHT,
    x: 0,
    y: 0,
    fontSize: 18,
    paddingHorizontal: 10,
    paddingVertical: 10,
    innerMargin: 5
  }, [
    [
      {
        displayName: 'ATTAQUE',
        value: Action.ATTACK
      },
      {
        displayName: 'SAC',
        value: Action.BAG
      }
    ],
    [
      {
        displayName: 'EQUIPE',
        value: Action.TEAM
      },
      {
        displayName: 'FUITE',
        value: Action.RUN
      }
    ]
  ], () => this.draw());

  constructor(x: number, y: number) {
    super({ x, y, width: ActionGroup.WIDTH, height: TextboxGroup.HEIGHT });
  }

  async init() {

    const background = await loadImage(`textboxes/secondary/default.jpg`, {
      x: 0,
      y: 0,
      width: this.width(),
      height: this.height()
    });

    this.add(background);
    this.add(this.menu);

    this.menu.load();
  }

  /** Resets the position of the cursor. */
  resetCursor() {
    this.menu.moveCursor(0, 0);
  }

  /** Navigates to the left. */
  navigate(direction: Direction) {
    this.menu.navigate(direction);
  }

  /** Selected menu item. */
  get selectedValue(): Action {
    return this.menu.selectedValue;
  }
}
