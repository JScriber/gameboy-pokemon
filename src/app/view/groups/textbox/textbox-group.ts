import Konva from 'konva';
import { Observable, Subject } from 'rxjs';
import { take, takeUntil, map } from 'rxjs/operators';
import { loadImage } from '../../utils/load-image';

// Assets.
import { TextboxStyle } from '../../assets/textbox-style';
import { SceneGroup } from '../scene/scene-group';
import { ActionGroup } from './action-group';

import { InputAggregator } from '../../input/input-aggregator';

import { Action } from '../../view';

// Models.
import { Attack } from '../../../models/Attack';
import { AttackGroup } from './attack-group';
import { Direction } from '../../shapes/menu/menu-shape';
import { Controllable } from './controllable.model';


export class TextboxGroup extends Konva.Group {

  public static readonly HEIGHT = 59;

  private static readonly TEXT_PADDING = 22;

  /** Message displayed by the textbox. */
  private readonly message = new Konva.Text({
    x: TextboxGroup.TEXT_PADDING,
    y: 9,
    width: SceneGroup.WIDTH - (2 * TextboxGroup.TEXT_PADDING),
    fontSize: 20,
    fontFamily: 'BattleFont',
    fontStyle: 'normal',
    fill: '#000'
  });

  private readonly actions = new ActionGroup(SceneGroup.WIDTH - ActionGroup.WIDTH, 0);

  private readonly attacks = new AttackGroup(0, 0);

  /** Input aggregator reference. */
  private input: InputAggregator;

  /**
   * Loads the textbox.
   * @param style
   */
  async load(style: TextboxStyle) {
    const textbox = await loadImage(`textboxes/${style}.png`, {
      x: 0,
      y: 0,
      width: 296,
      height: 59
    });

    await this.actions.init();
    this.actions.visible(false);

    await this.attacks.init();
    this.attacks.visible(false);
    
    this.add(textbox);
    this.add(this.message);
    this.add(this.actions);
    this.add(this.attacks);
  }

  /**
   * Shows the action menu and wait for a response.
   */
  showActionMenu(): Observable<Action> {

    // Initialize the selected action.
    this.showActions();

    // Reset the cursor position.
    this.actions.resetCursor();

    return this.control(this.actions, () => this.hideActions());
  }

  /**
   * Shows the attack menu and wait for a response.
   * @param attacks
   */
  showAttackMenu(attacks: Attack[]): Observable<Attack> {
    this.showAttacks();
    this.attacks.displayAttacks(attacks);

    return this.control(this.attacks, () => this.hideAttacks());
  }

  /**
   * Generic control over a controllable.
   * @template T - Returned type.
   * @param controllable - Controllable element.
   * @param beforeSubmit - Hook.
   */
  private control<T>(controllable: Controllable<T>, beforeSubmit: () => void): Observable<T> {

    // Outlet.
    const subscriptionHandler$ = new Subject();

    // Bind left input.
    this.input.left.pipe(takeUntil(subscriptionHandler$))
      .subscribe(() => controllable.navigate(Direction.LEFT));

    // Bind up input.
    this.input.up.pipe(takeUntil(subscriptionHandler$))
      .subscribe(() => controllable.navigate(Direction.UP));

    // Bind right input.
    this.input.right.pipe(takeUntil(subscriptionHandler$))
      .subscribe(() => controllable.navigate(Direction.RIGHT));

    // Bind down input.
    this.input.down.pipe(takeUntil(subscriptionHandler$))
      .subscribe(() => controllable.navigate(Direction.DOWN));

    // Bind A input.
    return this.input.a.pipe(
      take(1),
      map(() => {
        subscriptionHandler$.next();
        beforeSubmit();

        return controllable.selectedValue;
      })
    );
  }

  /** Shows the action component. */
  private showActions() {
    this.hideAttacks();

    // Change width of message.
    this.message.width(SceneGroup.WIDTH - ActionGroup.WIDTH - (2 * TextboxGroup.TEXT_PADDING));

    this.actions.visible(true);
    this.draw();
  }

  /** Hides the action component. */
  private hideActions() {
    // Change width of message.
    this.message.width(SceneGroup.WIDTH - (2 * TextboxGroup.TEXT_PADDING));

    this.actions.visible(false);
    this.draw();
  }

  /** Show the attack component. */
  private showAttacks() {
    this.hideActions();
    this.attacks.visible(true);
  }

  /** Hides the attacks component. */
  private hideAttacks() {
    this.attacks.visible(false);
    this.draw();
  }

  /**
   * Displays the message.
   * @param message
   */
  displayMessage(message: string) {
    this.message.text(message);
    this.draw();
  }

  /** Pass down the input to the sub components. */
  registerInput(input: InputAggregator) {
    this.input = input;
  }
}
