import Konva from 'konva';

// Groups.
import { SceneGroup } from '../scene/scene-group';
import { TextboxGroup } from './textbox-group';

import { loadImage } from '../../utils/load-image';
import { Attack } from '../../../models/Attack';
import { MenuShape, Direction } from '../../shapes/menu/menu-shape';

// Behaviours.
import { Controllable } from './controllable.model';
import { typeAssociations } from '../../text-associations/type-associations';

export class AttackGroup extends Konva.Group implements Controllable<Attack> {

  /** Attacks displayed. */
  private attacks: Attack[];

  private readonly menu = new MenuShape<string>({
    width: SceneGroup.WIDTH * (2 / 3),
    height: TextboxGroup.HEIGHT,
    x: 0,
    y: 0,
    fontSize: 16,
    textTopOffset: 1,
    paddingHorizontal: 15,
    paddingVertical: 10,
    innerMargin: 0
  }, null, () => this.draw());

  /** Type of the attack. */
  private readonly typeInformation = new Konva.Text({
    x: SceneGroup.WIDTH * (2 / 3) + 12,
    y: TextboxGroup.HEIGHT - 28,
    width: SceneGroup.WIDTH * (1 / 3) - 24,
    height: 15,
    fontSize: 17,
    fontFamily: 'BattleFont',
    fontStyle: 'bold',
    fill: '#000'
  });

  private readonly ppLabel = new Konva.Text({
    x: SceneGroup.WIDTH * (2 / 3) + 12,
    y: 10,
    fontSize: 17,
    fontFamily: 'BattleFont',
    fontStyle: 'bold',
    text: 'PP',
    fill: '#000'
  });

  /** PP left. */
  private readonly ppInformations = new Konva.Text({
    x: SceneGroup.WIDTH * (2 / 3) + 12,
    y: 10,
    width: SceneGroup.WIDTH * (1 / 3) - 24,
    fontSize: 17,
    fontFamily: 'BattleFont',
    fontStyle: 'bold',
    fill: '#000',
    align: 'right'
  });

  constructor(x: number, y: number) {
    super({ x, y, width: SceneGroup.WIDTH, height: TextboxGroup.HEIGHT });
  }

  async init() {

    const background = await loadImage(`textboxes/secondary/attack.jpg`, {
      x: 0,
      y: 0,
      width: this.width(),
      height: this.height()
    });

    this.add(background);
    this.add(this.menu);

    this.add(this.typeInformation);
    this.add(this.ppLabel);
    this.add(this.ppInformations);
  }

  /**
   * Displays the attacks of the pokemon.
   * @param attacks
   */
  displayAttacks(attacks: Attack[]) {

    // Keep knowledge of the attacks.
    this.attacks = attacks;

    this.menu.setValues([
      [
        {
          displayName: attacks[0].name.toUpperCase(),
          value: attacks[0].name
        },
        {
          displayName: attacks[1].name.toUpperCase(),
          value: attacks[1].name
        }
      ],
      [
        {
          displayName: attacks[2].name.toUpperCase(),
          value: attacks[2].name
        },
        {
          displayName: attacks[3].name.toUpperCase(),
          value: attacks[3].name
        }
      ]
    ]);

    this.menu.load();
    this.updateInformations();
  }

  /** Navigates to the left. */
  navigate(direction: Direction) {
    this.menu.navigate(direction);

    this.updateInformations();
  }

  /** Selected menu item. */
  get selectedValue(): Attack {
    const selected: string = this.menu.selectedValue;

    return this.attacks.find(a => a.name === selected);
  }

  /** Updates the informations of the selected attack. */
  private updateInformations() {
    const attack = this.selectedValue;

    this.ppInformations.text(`${attack.pp.current}/${attack.pp.base}`);
    this.typeInformation.text(`TYPE/${typeAssociations[attack.type].toUpperCase()}`);

    this.draw();
  }
}
