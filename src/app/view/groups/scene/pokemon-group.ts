import Konva from 'konva';
import { ContainerConfig } from 'konva/types/Container';

// Positioning.
import { RegionBox } from '../../utils/anchor/region-box';
import { Align, Anchor } from '../../utils/anchor/anchor';

// Shapes.
import { GifShape } from '../../shapes/gif/gif-shape';

/**
 * Pokemons displaying.
 */
export class PokemonGroup extends Konva.Group {

  /** Position of the foreground image. */
  private static readonly FOREGROUND_REGION = new RegionBox({
    align: Align.CENTER,
    distance: 0,
    origin: Anchor.LEFT
  }, {
    align: Align.END,
    distance: 0,
    origin: Anchor.BOTTOM
  }, {
    width: 180,
    height: 120
  }, 1.25);

  /** Position of the background image. */
  private static readonly BACKGROUND_REGION = new RegionBox({
    align: Align.CENTER,
    distance: 18,
    origin: Anchor.RIGHT
  }, {
    align: Align.END,
    distance: 0,
    origin: Anchor.TOP
  }, {
    width: 118,
    height: 93
  });

  private readonly foregroundImage = new Konva.Image();

  private readonly backgroundImage = new GifShape();

  constructor(config?: ContainerConfig) {
    super(config);

    this.add(this.foregroundImage);
    this.add(this.backgroundImage);

    // Plug positioning to GIF rendering.
    this.backgroundImage.render = () => PokemonGroup.BACKGROUND_REGION.attachPosition(this.backgroundImage, this);
  } 
  

  /**
   * Loads an image in the foreground.
   * @param image - Preloaded image.
   */
  loadForegroundImage(image: HTMLImageElement) {
    this.foregroundImage.image(image);

    PokemonGroup.FOREGROUND_REGION.attachPosition(this.foregroundImage, this);
  }

  /**
   * Loads an image in the background.
   * @param arrayBuffer
   */
  loadBackgroundImage(arrayBuffer: ArrayBuffer) {
    this.backgroundImage.loadBuffer(arrayBuffer);
  }

}
