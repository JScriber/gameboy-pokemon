import Konva from 'konva';
import { ContainerConfig } from 'konva/types/Container';

// Assets.
import { FieldStyle } from '../../assets/field-style';
import { PlatformStyle } from '../../assets/platform-style';

// Groups.
import { BackgroundGroup } from './background-group';
import { PokemonGroup } from './pokemon-group';
import { StatsGroup } from './stats-group';

// Model.
import { Pokemon } from '../../../models/Pokemon';
import { ImageBank, pkmnBackImageID, pkmnFrontImageID } from '../../utils/image-bank';

/** Position where the Pokemon must be displayed. */
export enum PokemonPosition {
  FOREGROUND,
  BACKGROUND
}

/**
 * Scene of the battlefield.
 * Contains the background and the platforms.
 */
export class SceneGroup extends Konva.Group {

  /** Width of a scene. */
  public static readonly WIDTH = 296;

  /** Height of a scene. */
  public static readonly HEIGHT = 156;

  /** Display the background. */
  private readonly background = new BackgroundGroup();

  /** Display the pokemons. */
  private readonly pokemons = new PokemonGroup({
    width: this.width(),
    height: this.height()
  });

  /** Display the stats. */
  private readonly stats = new StatsGroup();

  constructor(config?: ContainerConfig) {
    super(config);

    this.add(this.background);
    this.add(this.pokemons);
    this.add(this.stats);
  } 
  
  /**
   * Loads the scene with the given parameters.
   * @param field
   * @param platform
   */
  async load(field: FieldStyle, platform: PlatformStyle) {
    await this.background.load(field, platform);
  }

  /**
   * Displays the pokemon at the given position.
   * @param pokemon
   * @param position
   */
  displayPokemon(pokemon: Pokemon, position: PokemonPosition) {

    switch (position) {
      // Put the Pokemon in the foreground.
      case PokemonPosition.FOREGROUND:
        this.stats.displayForeground(pokemon);
        this.loadForegroundPokemonImage(pokemon);
        break;

      // Put the Pokemon in the background.
      case PokemonPosition.BACKGROUND:
        this.stats.displayBackground(pokemon);
        this.loadBackgroundPokemonImage(pokemon);
        break;
    }
  }

  /**
   * Displays the pokemon in the foreground.
   * @param pokemon 
   */
  private loadForegroundPokemonImage(pokemon: Pokemon) {
    const image = ImageBank.instance.accessImage(pkmnBackImageID(pokemon.graphicID));

    if (image) {
      this.pokemons.loadForegroundImage(image);
    } else {
      throw 'Failed to resolve the asset.';
    }
  }

  /**
   * Displays the pokemon in the background.
   * @param pokemon 
   */
  private loadBackgroundPokemonImage(pokemon: Pokemon) {
    const image = ImageBank.instance.accessBuffer(pkmnFrontImageID(pokemon.graphicID));

    if (image) {
      this.pokemons.loadBackgroundImage(image);
    } else {
      throw 'Failed to resolve the asset.';
    }
  }
}
