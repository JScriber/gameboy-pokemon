import Konva from 'konva';
import { ContainerConfig } from 'konva/types/Container';

// Shapes.
import { FullStatShape } from '../../shapes/stat/full-stat-shape';
import { MinStatShape } from '../../shapes/stat/min-stat-shape';

// Models.
import { Pokemon } from '../../../models/Pokemon';

/**
 * Stat displaying.
 */
export class StatsGroup extends Konva.Group {

  /** Foreground stat displayer. */
  private foregroundStat = new FullStatShape(168, 102);

  /** Background stat displayer. */
  private backgroundStat = new MinStatShape(5, 6);

  constructor(config?: ContainerConfig) {
    super(config);

    this.add(this.foregroundStat);
    this.add(this.backgroundStat);
  }

  /**
   * Displays the stats of the Pokemon.
   * @param pokemon
   */
  displayForeground(pokemon: Pokemon) {
    this.foregroundStat.displayStats(pokemon);
  }

  /**
   * Displays the stats of the Pokemon.
   * @param pokemon 
   */
  displayBackground(pokemon: Pokemon) {
    this.backgroundStat.displayStats(pokemon);
  }
}
