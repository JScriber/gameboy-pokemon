import Konva from 'konva';

// Assets.
import { FieldStyle } from '../../../view/assets/field-style';
import { PlatformStyle } from '../../../view/assets/platform-style';
import { loadImage } from '../../../view/utils/load-image';

// Parent.
import { SceneGroup } from './scene-group';

/**
 * Scene of the battlefield.
 * Contains the background and the platforms.
 */
export class BackgroundGroup extends Konva.Group {

  /**
   * Loads the scene with the given parameters.
   * @param field
   * @param platform
   */
  async load(field: FieldStyle, platform: PlatformStyle) {

    // Load the background.
    const background = await loadImage(`fields/${field}.png`, {
      x: 0,
      y: 0,
      width: SceneGroup.WIDTH,
      height: SceneGroup.HEIGHT
    });

    // Load the foreground platform.
    const foregroundPlatform = await loadImage(`platforms/${platform}/foreground.png`, {
      x: -40,
      y: 156 - 32
    });

    // Load the background platform.
    const backgroundPlatform = await loadImage(`platforms/${platform}/background.png`, {
      x: 155,
      y: 54
    });

    this.add(background);
    this.add(foregroundPlatform);
    this.add(backgroundPlatform);
  }
}
