import Konva from 'konva';

// Image access.
import { ImageBank, pkmnIconImageID } from '../../../utils/image-bank';

// Components.
import { PokeSlot } from './poke-slot';
import { HPGauge } from '../../../shapes/stat/hp-gauge';
import { GenderIndicator } from '../../../shapes/stat/gender-indicator';

// Model.
import { Pokemon } from '../../../../models/Pokemon';

/**
 * First pokemon representation in a team.
 */
export class FirstPokemon extends PokeSlot {


  /** Background of the slot. */
  private readonly background = new Konva.Image({
    x: 0,
    y: 0,
    width: 95,
    height: 60,
    image: undefined
  });

  /** Pokeball. */
  private readonly pokeball = new Konva.Image({
    x: -7,
    y: -7,
    width: 25,
    height: 25,
    image: undefined
  });

  /** Icon of the pokemon. */
  private readonly pokemonIcon = new Konva.Image({
    x: -3,
    y: 3,
    width: 30,
    height: 30,
    image: undefined
  });

  /** Name of the pokemon. */
  private readonly pokemonName = new Konva.Text({
    x: 30,
    y: 4,
    width: 60,
    align: 'left',
    fontSize: 16,
    fontFamily: 'BattleFont',
    fontStyle: '400',
    fill: '#fff'
  });

  /** Level of the pokemon. */
  private readonly pokemonLevel = new Konva.Text({
    x: 40,
    y: 19,
    width: 60,
    align: 'left',
    fontSize: 15,
    fontFamily: 'BattleFont',
    fontStyle: '400',
    fill: '#fff'
  });

  /** Gender of the pokemon. */
  private readonly pokemonGender = new GenderIndicator(82, 22);

  /** HP gauge. */
  private readonly hpGauge = new HPGauge(10, 34, 80);

  /** HP text. */
  private readonly hpText = new Konva.Text({
    x: 44,
    y: 44,
    width: 44,
    align: 'right',
    fontSize: 15,
    fontFamily: 'BattleFont',
    fontStyle: '400',
    fill: '#fff'
  });

  constructor(x: number, y: number) {
    super({ x, y });

    this.add(this.background);

    this.add(this.pokeball);

    this.add(this.hpGauge);
    this.add(this.hpText);

    this.add(this.pokemonName);
    this.add(this.pokemonLevel);
    this.add(this.pokemonGender);
    this.add(this.pokemonIcon);
  }

  /** Loads the images. */
  load() {

    // Load background.
    this.drawBackground();

    // Load pokeball.
    this.pokeball.image(ImageBank.instance.accessImage('team-pokeball'));
  }

  /**
   * Displays the given pokemon.
   * @param pokemon
   */
  display(pokemon: Pokemon) {

    this.pokemonName.text(pokemon.name.toUpperCase());
    this.pokemonLevel.text(`N.${pokemon.level}`);
    this.pokemonGender.display(pokemon.gender);
    this.pokemonIcon.image(ImageBank.instance.accessImage(pkmnIconImageID(pokemon.graphicID)));

    this.hpGauge.displayHP(pokemon.hp);
    this.hpText.text(`${pokemon.hp.current}/${pokemon.hp.base}`);
  }

  /** @inheritdoc */
  protected onSelectionChange() {
    this.drawBackground();

    if (this._selected) {
      this.pokeball.image(ImageBank.instance.accessImage('team-pokeball-selected'));
      this.pokeball.height(32);
      this.pokeball.y(-12);
    } else {
      this.pokeball.image(ImageBank.instance.accessImage('team-pokeball'));
      this.pokeball.height(25);
      this.pokeball.y(-7);
    }
  }

  /**
   * Draws the background of the slot.
   */
  private drawBackground() {

    // ID of the image to display.
    let image: string;

    if (this._selected) {
      image = 'team-first-pokemon-selected';
    } else {
      image = 'team-first-pokemon';
    }

    this.background.image(ImageBank.instance.accessImage(image));
  }

}
