import Konva from 'konva';

/**
 * Displays the information of a Pokemon in what is called a "slot".
 * In this slot the informations displayed are the following : 
 * - The name ;
 * - The level ;
 * - The gender ; 
 * - The HP ;
 * - The icon representing the Pokemon.
 */
export abstract class PokeSlot extends Konva.Group {

  /** Says if the slot is selected. */
  protected _selected = false;

  /**
   * Changes the selection state of the slot.
   * @param {boolean} value - New state.
   */
  public set selected(value: boolean) {
    this._selected = value;

    console.log(value);

    this.onSelectionChange();
  }

  /**
   * Called whenever the selection changes.
   */
  protected abstract onSelectionChange(): void;

}
