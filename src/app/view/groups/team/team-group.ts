import Konva from 'konva';
import { ContainerConfig } from 'konva/types/Container';
import { ImageBank } from '../../utils/image-bank';
import { View } from '../../view';
import { FirstPokemon } from './components/first-pokemon';
import { Pokemon } from '../../../models/Pokemon';

export class TeamGroup extends Konva.Group {

  /** Background of the view. */
  private readonly background = new Konva.Image({
    x: -2,
    width: View.WIDTH,
    height: View.HEIGHT,
    image: undefined
  });

  /** First pokemon slot. */
  private readonly firstPokemon = new FirstPokemon(12, 30);

  constructor(config?: ContainerConfig) {
    super(config);

    this.add(this.background);

    this.add(this.firstPokemon);
  }

  /** Loads the assets. */
  load() {

    // Background of the view.
    this.background.image(ImageBank.instance.accessImage('team-background'));

    // Load the first pokemon slot.
    this.firstPokemon.load();

  }

  /**
   * Displays the team.
   * @param team
   */
  display(team: Pokemon[]) {

    this.firstPokemon.display(team[0]);

    let selected = true;

    setInterval(() => {
      selected = !selected;
      this.firstPokemon.selected = selected;
      this.draw();
    }, 1000);

  }
}
