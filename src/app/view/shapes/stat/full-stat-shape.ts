import Konva from 'konva';

import { StatShape, font } from './stat-shape';

// Model.
import { Pokemon } from '../../../models/Pokemon';

/**
 * Displays all the stats of a Pokemon.
 */
export class FullStatShape extends StatShape {


  /** HP left to the pokemon. */
  private readonly pokemonHP = new Konva.Text({
    x: 15, y: 32, ... font,
    width: 100,
    align: 'right'
  });

  constructor(x: number, y: number) {
    super(x, y, 122, 48);

    this.add(this.pokemonHP);
  }

  /** @inheritdoc */
  protected decoration() {
    return new Konva.Line({
      points: [0, 35, -15, 52, 105, 52, 115, 48],
      fill: '#566560',
      closed: true
    });
  }

  /**
   * Displays the stats of the Pokemon.
   * @param pokemon
   */
  async displayStats(pokemon: Pokemon) {
    super.displayStats(pokemon);

    this.pokemonHP.text(`${pokemon.hp.current}/${pokemon.hp.base}`);
  }
}
