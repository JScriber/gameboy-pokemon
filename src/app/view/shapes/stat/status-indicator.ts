import Konva from 'konva';

// Model.
import { Status } from '../../../models/Status';

interface Style {
  text: string;
  color: string;
}

/**
 * Style association to each status.
 */
const statusStyle: { [key: number]: Style } = {
  [Status.PARALYSED]: {
    text: 'PAR',
    color: '#accc2a'
  },
  [Status.BURNT]: {
    text: 'BRN',
    color: '#ec5f4e'
  },
  [Status.POISONED]: {
    text: 'PSN',
    color: '#d300bc'
  },
  [Status.ASLEEP]: {
    text: 'SLP',
    color: '#9ca587'
  }
};

/**
 * Indicates the current status.
 */
export class StatusIndicator extends Konva.Group {

  private readonly background = new Konva.Rect({
    x: 0,
    y: 0,
    width: this.width(),
    height: this.height(),
    cornerRadius: 3
  });

  private readonly text = new Konva.Text({
    x: 0,
    y: -1,
    width: this.width(),
    height: this.height(),
    align: 'center',
    verticalAlign: 'center',
    fontSize: 13,
    fontFamily: 'BattleFont',
    fontStyle: 'bold',
    fill: '#fff'
  });

  constructor(x: number, y: number) {
    super({ x, y, width: 25, height: 12 });

    this.add(this.background);
    this.add(this.text);
  }

  /**
   * Displays the given status.
   * @param status
   */
  display(status: Status) {

    const differentNode = status !== Status.NONE;

    this.visible(differentNode);

    if (differentNode) {
      const style = statusStyle[status];
  
      this.background.fill(style.color);
      this.text.text(style.text);
    }
  }
}
