import Konva from 'konva';

import { StatShape } from './stat-shape';

/**
 * Displays all the stats of a Pokemon.
 */
export class MinStatShape extends StatShape {

  constructor(x: number, y: number) {
    super(x, y, 130, 38);

    this.status.y(20);
    this.hpGauge.x(36);
    this.hpGauge.y(22);
  }

  /** @inheritdoc */
  protected decoration() {
    return new Konva.Line({
      points: [130, 30, 138, 43, 10, 43, 0, 35],
      fill: '#566560',
      closed: true
    });
  }
}
