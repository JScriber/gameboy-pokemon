import Konva from 'konva';

// Shapes.
import { HPGauge } from './hp-gauge';
import { StatusIndicator } from './status-indicator';
import { GenderIndicator } from './gender-indicator';

// Models.
import { Pokemon } from '../../../models/Pokemon';

/** Font used everywhere. */
export const font = {
  fontSize: 15,
  fontFamily: 'BattleFont',
  fontStyle: 'bold',
  fill: '#000',
  shadowColor: '#d8d0b0',
  shadowBlur: 0,
  shadowOffset: { x: 1, y: 1 },
  shadowOpacity: 1
};

/**
 * Displays all the stats of a Pokemon.
 */
export abstract class StatShape extends Konva.Group {

  /** Name of the pokemon. */
  private readonly pokemonName = new Konva.Text({ x: 7, y: 3, ... font });

  /** Level of the pokemon. */
  private readonly pokemonLevel = new Konva.Text({
    x: 80, y: 3, ... font,
    width: 35,
    align: 'right'
  });

  /** Gender. */
  private readonly gender = new GenderIndicator(69, 5);

  /** Gauge. */
  protected readonly hpGauge = new HPGauge(27, 20);

  /** Status. */
  protected readonly status = new StatusIndicator(7, 32);

  constructor(x: number, y: number, width: number, height: number) {
    super({ x, y, width, height });

    // Decoration.
    const background = new Konva.Rect({
      x: 0,
      y: 0,
      width: this.width(),
      height: this.height(),
      fill: '#f8f8d8',
      stroke: '#203800',
      strokeWidth: 2,
      cornerRadius: [9, 5, 9, 5]
    });

    this.add(this.decoration());
    this.add(background);

    this.add(this.pokemonName);
    this.add(this.pokemonLevel);
    this.add(this.hpGauge);
    this.add(this.status);
    this.add(this.gender);
  }

  protected abstract decoration(): Konva.Shape;

  /**
   * Displays the stats of the Pokemon.
   * @param pokemon
   */
  async displayStats(pokemon: Pokemon) {
    this.pokemonName.text(pokemon.name.toUpperCase());
    this.pokemonLevel.text('N.' + pokemon.level);

    this.hpGauge.displayHP(pokemon.hp);
    this.status.display(pokemon.status);
    this.gender.display(pokemon.gender);
    this.gender.x(this.pokemonName.width() + 11);
  }
}
