import Konva from 'konva';
import { Gender } from '../../../models/gender';
import { ImageBank } from '../../utils/image-bank';

/**
 * Display the gender.
 */
export class GenderIndicator extends Konva.Image {

  constructor(x: number, y: number) {
    super({
      x, y,
      height: 10,
      width: 7,
      image: null,
      visible: false
    });
  }

  /**
   * Displays the given gender.
   * @param gender
   */
  display(gender: Gender) {
    const hasGender = gender !== Gender.NONE;

    if (hasGender) {
      const key = gender === Gender.MALE ? 'male' : 'female';

      this.image(ImageBank.instance.accessImage(key));
    }

    this.visible(hasGender);
  }
}
