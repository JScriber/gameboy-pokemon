import Konva from 'konva';

// Models.
import { Stat } from '../../../models/Stat';

enum State {
  BADLY_INJURED = '#a80909',
  INJURED = '#fdbb43',
  HEALTHY = '#95e3a5'
}

/**
 * Displays the HP in a gauge.
 */
export class HPGauge extends Konva.Group {

  /** Width of the shape. */
  private static readonly WIDTH = 89;

  /** Height of the shape. */
  private static readonly HEIGHT = 9;

  /** Progres bar. */
  private readonly progress = new Konva.Rect({
    x: 18,
    y: 2,
    height: 5,
    stroke: '#fff',
    strokeWidth: 1,
    cornerRadius: 3
  });

  /**
   * Constructor.
   * @param x - X position.
   * @param y - Y position.
   */
  constructor(x: number, y: number, gaugeWidth = HPGauge.WIDTH) {
    super({
      x, y,
      width: gaugeWidth,
      height: HPGauge.HEIGHT
    });

    // Background of the gauge.
    const background = new Konva.Rect({
      x: 0,
      y: 0,
      width: this.width(),
      height: this.height(),
      fill: '#505050',
      cornerRadius: 5
    });

    // Progress bar background.
    const fullProgress = new Konva.Rect({
      x: 18,
      y: 2,
      height: 5,
      width: this.width() - 20,
      fill: '#707070',
      stroke: '#fff',
      strokeWidth: 1,
      cornerRadius: 3
    });

    // Label "HP".
    const label = new Konva.Text({
      x: 4,
      y: -1,
      fontSize: 12,
      fontFamily: 'BattleFont',
      fillLinearGradientStartPoint: { x: 50, y: 0 },
      fillLinearGradientEndPoint: { x: 50, y: 18 },
      fillLinearGradientColorStops: [0, '#eeb27b', 1, '#c56d4e'],
      fontStyle: '900',
      text: 'PV'
    });

    // Progress bar.
    const width = this.width() - 20;
    this.progress.width(width); 

    this.add(background);
    this.add(label);
    this.add(fullProgress);
    this.add(this.progress);
  }

  /**
   * Displays the HP.
   * @param hp
   */
  displayHP(hp: Stat) {
    const ratio =  hp.current / hp.base;
    let width = (this.width() - 20) * ratio;

    let state: State;

    if (ratio > 0.4) {
      state = State.HEALTHY;
    } else {
      if (ratio > 0.2) {
        state = State.INJURED;
      } else {
        state = State.BADLY_INJURED;
      }
    }

    this.progress.fill(state);

    // TODO: Add animation.
    this.progress.width(width);
  }
}
