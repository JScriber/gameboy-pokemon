import Konva from 'konva';
import { Shape, ShapeConfig } from 'konva/types/Shape';
import { ImageBank } from '../../utils/image-bank';

// Behaviours.
import { Controllable } from '../../groups/textbox/controllable.model';

interface Settings {
  x: number;
  y: number;
  width: number;
  height: number;
  paddingHorizontal: number;
  paddingVertical: number;
  innerMargin: number;
  fontSize: number;
  textTopOffset?: number;
}

interface MenuValue<T> {
  displayName: string;
  value: T;
}

/**
 * Navigation directions.
 */
export enum Direction {
  UP,
  LEFT,
  RIGHT,
  DOWN
}

/**
 * Selection menu implementation.
 * Not related to any input events.
 */
export class MenuShape<T> extends Konva.Group implements Controllable<T> {

  /** Physical cursor representation. */
  private cursor: Shape<ShapeConfig>;

  /** Position of the cursor. */
  private cursorPosition: [number, number] = [0, 0];

  /** Vertical padding. */
  private paddingVertical: number;

  /** Horizontal padding. */
  private paddingHorizontal: number;

  /** Size of the font. */
  private fontSize: number;

  /** Top offset of the text. */
  private textTopOffset: number;

  /** Height of a label. */
  private labelHeight = 21;

  /** Left padding for cursor. */
  private readonly cursorShunt = 10;

  /** Inner space between items. */
  private readonly innerMargin: number;

  /** Known labels (recycle use). */
  private readonly labels: { [key: string]: Konva.Text } = {};

  /**
   * Constructor.
   * @param settings - Settings of the menu.
   * @param values - Values displayed in the menu.
   * @param forceDraw - Scope access to parent draw function.
   */
  constructor({ x, y, width, height, fontSize, textTopOffset, paddingHorizontal, paddingVertical, innerMargin }: Settings,
              private values: MenuValue<T>[][],
              private readonly forceDraw: () => void) {
    super({ x, y, width, height });

    if (values === null || (values.length === 2 && values[0].length === 2)) {
      this.paddingHorizontal = paddingHorizontal;
      this.paddingVertical = paddingVertical;
      this.fontSize = fontSize;
      this.textTopOffset = textTopOffset || 0;
      this.innerMargin = innerMargin;
    } else {
      throw 'You must give a 2 by 2 matrix.';
    }
  }

  /**
   * Loads the component.
   * Note: It must be injected in a layer beforehand.
   */
  load() {
    this.buildLabels();
    this.buildCursor();
  }

  /** @inheritdoc */
  navigate(direction: Direction): void {

    // Convert the direction into a vector.
    let vector = [0, 0];

    switch (direction) {
      case Direction.LEFT:
        vector = [-1, 0];
        break;

      case Direction.UP:
        vector = [0, -1];
        break;

      case Direction.RIGHT:
        vector = [1, 0];
        break;

      case Direction.DOWN:
        vector = [0, 1];
        break;
    }

    // Calculate the new position.
    const x = this.cursorPosition[0] + vector[0];
    const y = this.cursorPosition[1] + vector[1];

    this.moveCursor(x, y);
  }

  /**
   * Selected value in the menu.
   */
  get selectedValue(): T {
    const [x, y] = this.cursorPosition;

    return this.values[y][x].value;
  }

  /**
   * Sets the values of the menu.
   * @param values
   */
  setValues(values: MenuValue<T>[][]) {
    this.cursorPosition = [0, 0];

    this.values = values;
  }

  /**
   * Virtually moves the cursor.
   * @param x
   * @param y 
   */
  moveCursor(x: number, y: number) {
    // Test the validity of the position.
    if (x >= 0 && x < this.values[0].length && y >= 0 && y < this.values.length) {
      this.cursorPosition = [x, y];

      // Move and redraw the scene.
      this.moveGraphicalCursor();
      this.forceDraw();
    }
  }

  /**
   * Draws the cursor at its virtual coordinates.
   */
  private moveGraphicalCursor() {
    const width = this.singleItemWidth;
    const height = this.labelHeight;
    const [x, y] = this.cursorPosition;

    let [posX, posY] = this.getGraphicalPosition(x, y, width, height);

    // Remove shunt has we retrieved the position of the labels.
    posX -= this.cursorShunt;

    // Center the cursor.
    posY += (height / 2) - (this.cursor.height() / 2) - 2;

    this.cursor.x(posX);
    this.cursor.y(posY);
  }

  /**
   * Setups the cursor.
   */
  private buildCursor() {

    if (this.cursor === undefined) {
      // Image of the cursor.
      const cursorImage = ImageBank.instance.accessImage('cursor');
  
      if (cursorImage !== undefined) {
  
        this.cursor = new Konva.Image({
          image: cursorImage,
          width: 8,
          height: 12
        });
  
        this.add(this.cursor);
        this.moveGraphicalCursor();
      }
    } else {
      this.moveGraphicalCursor();
    }
  }

  /**
   * Initializes the labels position.
   */
  private buildLabels() {

    // Counts.
    const xCount = this.values[0].length;
    const yCount = this.values.length;

    // Dimensions.
    const width = this.singleItemWidth;
    const height = this.labelHeight;

    // Apply the positions for each label.
    for (let x = 0; x < xCount; x ++) {
      for (let y = 0; y < yCount; y ++) {
        const [posX, posY] = this.getGraphicalPosition(x, y, width, height);

        const id = `${x}:${y}`;
        const name = this.values[y][x].displayName;
        const label = this.labels[id];

        if (label) {
          label.text(name);
        } else {
          this.labels[id] = this.addLabel(posX, posY, width, height, name);
        }
      }
    }
  }

  /**
   * Width of a single element.
   */
  private get singleItemWidth() {
    const xCount = this.values[0].length;

    return (this.width() -
      (2 * this.paddingHorizontal) - (xCount * this.cursorShunt) -
      (xCount - 1) * this.innerMargin) / xCount;
  }

  /**
   * Calculates the width of a given element in the graphical representation.
   * @param x - Virtual X position.
   * @param y - Virtual Y position.
   * @param width - Average width. Given to avoid useless calculation.
   * @param height - Average height. Given to avoid useless calculation.
   * @returns the graphical X and Y position.
   */
  private getGraphicalPosition(x: number, y: number, width: number, height: number): [number, number] {
    const margin = x === 0 ? 0 : this.innerMargin;
    const posX = this.paddingHorizontal + (x * width) + ((x + 1) * this.cursorShunt) + margin;
    const posY = this.paddingVertical + y * height;

    return [posX, posY];
  }

  /**
   * Creates an action label.
   */
  private addLabel(x: number, y: number, width: number, height: number, text: string): Konva.Text {

    y += this.textTopOffset;

    const label = new Konva.Text({
      x, y, width, height,
      fontSize: this.fontSize,
      fontFamily: 'BattleFont',
      fontStyle: 'bold',
      fill: '#000'
    });

    label.text(text);
    this.add(label);

    return label;
  }
}
