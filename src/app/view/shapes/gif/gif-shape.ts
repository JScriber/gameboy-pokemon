import Konva from 'konva';
import { ShapeConfig } from 'konva/types/Shape';
import { GifPlayer } from './gif-player';


interface GifConfig extends ShapeConfig {
  arrayBuffer: ArrayBuffer;
  autoPlay?: boolean;
}

/**
 * Konva image specialization for GIF.
 * Provides a custom framework encapsulation for the GIF player.
 */
export class GifShape extends Konva.Image {

  /** Rendering hook. */
  render: () => void = () => {};

  /** GIF player. */
  private readonly player = new GifPlayer(() => {
    this.render();
    this.getLayer().draw();
  });

  private readonly autoPlay: boolean;

  constructor(config?: GifConfig) {
    super({ image: null, ... config });

    this.autoPlay = (config && config.autoPlay) || true;

    this.image(this.player.image);

    if (config) this.loadBuffer(config.arrayBuffer);
  }

  /** Changes the GIF buffer on the fly. */
  loadBuffer(data: ArrayBuffer) {
    this.player.load(data, this.autoPlay);
  }

  play() {
    this.player.play();
  }

  pause() {
    this.player.pause();
  }
}
