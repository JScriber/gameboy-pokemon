
import { GIF, Gif } from 'gif-engine-js';

/**
 * Own implementation of a GIF player.
 */
export class GifPlayer {

  /** Canvas the GIF is painted on. */
  private readonly canvas = document.createElement('canvas');

  /** Drawing context. */
  private readonly context = this.canvas.getContext('2d');

  /** GIF informations. */
  private gifObject: Gif;

  /** Current frame displayed. */
  private frameIndex = 0;

  /** Cycling process. */
  private process: NodeJS.Timeout;

  /** Says if the player is paused. */
  private paused = true;

  constructor(private readonly layerReloader: () => void) {}

  /**
   * Loads the GIF at the given URL.
   * @param path to the GIF.
   * @param autoPlay - Says if the GIF should play.
   */
  async load(arrayBuffer: ArrayBuffer, autoPlay: boolean) {
    this.gifObject = await GIF(arrayBuffer);
    this.reset();

    if (autoPlay) this.play();
  }

  /**
   * Starts the GIF.
   */
  play() {
    if (this.paused) {
      this.paused = false;
      this.cycle();
    }
  }

  /**
   * Pauses the GIF.
   */
  pause() {
    if (!this.paused) {
      this.paused = true;
      clearTimeout(this.process);
    }
  }

  /**
   * External representation.
   */
  get image() {
    return this.canvas;
  }

  /**
   * Cycles through the frames.
   */
  private cycle() {
    const nextIndex = this.nextIndex;
    const nextFrame = this.gifObject.frames[nextIndex];

    // Draw current frame.
    this.draw();

    this.process = setTimeout(() => {
      this.frameIndex = nextIndex;
      this.cycle();
    }, nextFrame.graphicExtension.delay);
  }

  /**
   * Renders the current frame.
   */
  private async draw() {
    const frame = this.currentFrame;

    this.canvas.width = frame.descriptor.width;
    this.canvas.height = frame.descriptor.height;

    const [imageData] = await this.gifObject.toImageData(this.frameIndex);

    this.context.putImageData(imageData, 0, 0);

    // Force redrawing.
    this.layerReloader();
  }

  /**
   * Returns the index of the next frame.
   * @returns {number}
   */
  private get nextIndex() {
    let next = this.frameIndex + 1;

    // TODO: Check if the GIF must be repeated.
    if (next > this.gifObject.frames.length - 1) {
      next = 0;
    }

    return next;
  }

  /**
   * Finds the current frame.
   */
  private get currentFrame() {
    return this.gifObject.frames[this.frameIndex];
  }

  /**
   * Resets the GIF player.
   */
  private reset() {
    this.pause();
    this.frameIndex = 0;
  }
}
