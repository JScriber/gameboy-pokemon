import { Type } from '../../models/Type';

export const typeAssociations: { [key: number]: string } = {
  [Type.BUG]: 'Insecte',
  [Type.DARK]: 'Tenebre',
  [Type.DRAGON]: 'Dragon',
  [Type.ELECTRIC]: 'Electrique',
  [Type.FAIRY]: 'Fee',
  [Type.FIGHTING]: 'Combat',
  [Type.FIRE]: 'Feu',
  [Type.FLYING]: 'Vol',
  [Type.GHOST]: 'Spectre',
  [Type.GRASS]: 'Plante',
  [Type.GROUND]: 'Sol',
  [Type.ICE]: 'Glace',
  [Type.NORMAL]: 'Normal',
  [Type.POISON]: 'Poison',
  [Type.PSYCHIC]: 'Psy',
  [Type.ROCK]: 'Roche',
  [Type.STEEL]: 'Acier',
  [Type.WATER]: 'Eau'
};
