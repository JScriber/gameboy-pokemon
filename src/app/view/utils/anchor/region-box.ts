import Konva from 'konva';

import { HorizontalAnchor, VerticalAnchor, Dimensions, Align, Position, Anchor } from './anchor';

export class RegionBox {

  constructor(private readonly horizontal: HorizontalAnchor,
              private readonly vertical: VerticalAnchor,
              private readonly dimensions: Dimensions,
              private readonly scaling?: number) {}

  attachPosition(node: Konva.Node, group: Konva.Group) {

    const start = this.startPoint(group.width(), group.height());

    // Apply scaling.
    if (this.scaling) {
      node.width(node.width() * this.scaling);
      node.height(node.height() * this.scaling);
    }

    let wNode = node.width();
    let hNode = node.height();
    let x, y;

    // Horizontal alignment.
    switch (this.horizontal.align) {
      case Align.START:
        x = start.x;
        break;

      case Align.CENTER:
        x = start.x + (this.dimensions.width / 2) - (wNode / 2);
        break;

      case Align.END:
        x = start.x + this.dimensions.width - wNode;
        break;
    }

    // Vertical alignment.
    switch (this.vertical.align) {
      case Align.START:
        y = start.y;
        break;

      case Align.CENTER:
        y = start.y + (this.dimensions.height / 2) - (hNode / 2);
        break;

      case Align.END:
        y = start.y + this.dimensions.height - hNode;
        break;
    }

    node.x(x);
    node.y(y);
  }

  /**
   * Constraint box debugging.
   */
  debug(group: Konva.Group) {

    const start = this.startPoint(group.width(), group.height());

    const rect = new Konva.Rect({
      x: start.x,
      y: start.y,
      width: this.dimensions.width,
      height: this.dimensions.height,
      stroke: 'red',
      strokeWidth: 1
    });

    group.add(rect);
  }

  /**
   * Position of the start point.
   * @param width
   * @param height
   */
  private startPoint(width: number, height: number): Position {

    let x, y;

    // Determine X value.
    if (this.horizontal.origin === Anchor.LEFT) {
      x = this.horizontal.distance;
    } else {
      x = width - this.dimensions.width - this.horizontal.distance;
    }

    // Determine Y value.
    if (this.vertical.origin === Anchor.TOP) {
      y = this.vertical.distance;
    } else {
      y = height - this.dimensions.height - this.vertical.distance;
    }

    return { x, y };
  }
}
