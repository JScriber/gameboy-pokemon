export enum Anchor {
  LEFT,
  TOP,
  RIGHT,
  BOTTOM
}

export enum Align {
  START,
  CENTER,
  END
}

interface PositionAnchor {
  distance: number;
  align: Align;
}

/** Vertical anchor. */
export interface VerticalAnchor extends PositionAnchor {
  origin: Anchor.TOP | Anchor.BOTTOM;
}

/** Horizontal box. */
export interface HorizontalAnchor extends PositionAnchor {
  origin: Anchor.LEFT | Anchor.RIGHT;
}

export interface Dimensions {
  width: number;
  height: number;
}

export interface Position {
  x: number;
  y: number;
}
