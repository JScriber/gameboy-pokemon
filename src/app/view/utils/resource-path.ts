/**
 * Builds the path to image resource.
 * @param path
 */
export const resourcePath = path => `/images/${path}`;
