import { resourcePath } from './resource-path';

/** ID generator for back image. */
export const pkmnBackImageID = graphicID => `pkmn-back:${graphicID}`;

/** ID generator for front image. */
export const pkmnFrontImageID = graphicID => `pkmn-front:${graphicID}`;

/** ID generator for icon image. */
export const pkmnIconImageID = graphicID => `pkmn-icon:${graphicID}`;


/**
 * Asset representation.
 */
export interface Asset {
  id: string;
  source: string;
}

/**
 * Image loading and access.
 */
export class ImageBank {

  /** Unique class instance. */
  private static _instance: ImageBank;

  static get instance() {
    if (ImageBank._instance === undefined)
      ImageBank._instance = new this();

    return ImageBank._instance;
  }

  private constructor() {}

  /** List of loaded assets. */
  private imageAssets: { [key: string]: HTMLImageElement } = {};

  /** List of loaded GIF. */
  private bufferAssets: { [key: string]: ArrayBuffer } = {};

  /**
   * Access a loaded resource by its id.
   * @param id
   */
  accessImage(id: string): HTMLImageElement | undefined {
    if (Object.keys(this.imageAssets).length === 0) {
      console.warn('You must load your assets before accessing them.');
    } else {
      const asset = this.imageAssets[id];
  
      if (asset) return asset;
    }
  }

  /**
   * Access a loaded GIF by its id.
   * @param id
   */
  accessBuffer(id: string): ArrayBuffer | undefined {
    if (Object.keys(this.bufferAssets).length === 0) {
      console.warn('You must load your buffer assets before accessing them.');
    } else {
      const asset = this.bufferAssets[id];
  
      if (asset) return asset;
    }
  }

  /**
   * Loads the list of assets.
   * @param assets
   */
  async load(assets: Asset[]) {
    assets.forEach(async ({ id, source }) => {

      if (RegExp('.+\.gif').test(source)) {
        this.bufferAssets[id] = await this.loadGif(source);
      } else {
        this.imageAssets[id] = await this.loadImage(source);
      }
    });
  }

  /**
   * Loads the image with the given path.
   * @param source
   */
  private loadImage(source: string): Promise<HTMLImageElement> {

    return new Promise(r => {
      const image = new Image();

      image.onload = () => r(image);
      image.src = resourcePath(source);
    });
  }

  /**
   * Loads the GIF with the given path.
   * @param source
   */
  private loadGif(source: string): Promise<ArrayBuffer> {

    return fetch(resourcePath(source)).then(r => r.arrayBuffer());
  }
}
