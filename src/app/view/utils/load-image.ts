import Konva from 'konva';
import { ShapeConfig, Shape } from 'konva/types/Shape';
import { resourcePath } from './resource-path';

/**
 * Loads an image.
 * @param path
 * @param attributes
 * @deprecated Use image bank instead.
 */
export function loadImage(path: string, attributes: any) {
  const resource = resourcePath(path);

  return new Promise<Shape<ShapeConfig>>(r => Konva.Image.fromURL(resource, function(img) {
    img.setAttrs(attributes);
    r(img);
  }));
}
