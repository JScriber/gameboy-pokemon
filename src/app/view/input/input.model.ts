import { Observable } from 'rxjs';

/** Input contract. */
export interface Input {

  a: Observable<void>;

  b: Observable<void>;

  left: Observable<void>;

  up: Observable<void>;

  right: Observable<void>;

  down: Observable<void>;

}
