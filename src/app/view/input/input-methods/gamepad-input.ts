import { InputMethod } from '../input-method';

type Controllers = { [key: number]: Gamepad };

/**
 * Gamepad input method implementation.
 */
export class GamepadInput extends InputMethod {

  private static readonly SCAN_RATE = 500;

  /** Registered controllers. */
  private controllers: Controllers = {};

  constructor() {
    super();

    if (this.hasEvent) {
      window.addEventListener('gamepadconnected', (e: GamepadEvent) => this.addGamepad(e.gamepad));

      window.addEventListener('gamepaddisconnected', (e: GamepadEvent) => this.removeGamepad(e.gamepad));
    } else {
      setInterval(() => this.scan(), GamepadInput.SCAN_RATE);
    }
  }

  /**
   * Says if the browser supports gamepad related events.
   */
  private get hasEvent() {
    return 'GamepadEvent' in window;
  }

  /**
   * Registers a new gamepad.
   * @param gamepad
   */
  private addGamepad(gamepad: Gamepad) {

    console.log('Add gamepad', gamepad);

    this.controllers[gamepad.index] = gamepad;

    window.requestAnimationFrame(() => this.update());
  }

  /**
   * Removes a registered gamepad.
   * @param gamepad
   */
  private removeGamepad(gamepad: Gamepad) {
    delete this.controllers[gamepad.index];
  }

  private update() {

    for (let index in this.controllers) {
      const controller = this.controllers[index];

      controller.buttons.forEach(button => {
        console.log(button);
      });
    }
  }

  /** Scan for gamepads. */
  private scan() {

    // TODO: better webkit support.
    const gamepads = navigator.getGamepads();

    gamepads.forEach(gamepad => {
      if (!(gamepad.index in this.controllers)) {
        this.addGamepad(gamepad);
      } else {
        this.controllers[gamepad.index] = gamepad;
      }
    });
  }
}
