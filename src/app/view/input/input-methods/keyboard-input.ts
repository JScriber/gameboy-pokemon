import { InputMethod } from '../input-method';

const keys = {
  A: 65,
  B: 66,

  // First set of arrows.
  Z: 90,
  Q: 81,
  S: 83,
  D: 68,

  // Second set of arrows.
  ARROW_LEFT: 37,
  ARROW_UP: 38,
  ARROW_RIGHT: 39,
  ARROW_DOWN: 40
};

/**
 * Keyboard method implementation.
 */
export class KeyboardInput extends InputMethod {

  constructor() {
    super();

    window.addEventListener('keyup', e => {

      switch (e.which) {
        case keys.A:
          this._a.next();
          break;

        case keys.B:
          this._b.next();
          break;

        case keys.ARROW_LEFT:
        case keys.Q:
          this._left.next();
          break;

        case keys.ARROW_UP:
        case keys.Z:
          this._up.next();
          break;

        case keys.ARROW_RIGHT:
        case keys.D:
          this._right.next();
          break;

        case keys.ARROW_DOWN:
        case keys.S:
          this._down.next();
          break;

        default:
          // Key not mapped.
          break;
      }
    });
  }
}
