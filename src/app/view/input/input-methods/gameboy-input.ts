import { InputMethod } from '../input-method';

/**
 * Gameboy input method implementation.
 */
export class GameboyInput extends InputMethod {

  constructor() {
    super();

    // A button mapping.
    document.getElementById('button_a').addEventListener('click', () => this._a.next());

    // B button mapping.
    document.getElementById('button_b').addEventListener('click', () => this._b.next());

    // LEFT button mapping.
    document.getElementById('button_left').addEventListener('click', () => this._left.next());

    // UP button mapping.
    document.getElementById('button_up').addEventListener('click', () => this._up.next());

    // RIGHT button mapping.
    document.getElementById('button_right').addEventListener('click', () => this._right.next());

    // DOWN button mapping.
    document.getElementById('button_down').addEventListener('click', () => this._down.next());
    
  }
}
