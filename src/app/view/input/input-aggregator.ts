import { Observable, merge } from 'rxjs';
import { Input } from './input.model';

/**
 * Input methods aggregator.
 */
export class InputAggregator implements Input {

  a = this.aggregate('a');

  b = this.aggregate('b');

  left = this.aggregate('left');

  up = this.aggregate('up');

  right = this.aggregate('right');

  down = this.aggregate('down');

  constructor(private readonly inputs: Input[]) {}

  /**
   * Aggregates the streams.
   * @param key
   */
  private aggregate(key: keyof Input): Observable<void> {
    return merge(... this.inputs.map(i => i[key]));
  }
}
