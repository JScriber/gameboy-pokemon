import { Input } from './input.model';
import { Subject } from 'rxjs';

/**
 * Basic input method implementation.
 */
export abstract class InputMethod implements Input {

  get a() {
    return this._a.asObservable();
  }

  get b() {
    return this._b.asObservable();
  }

  get left() {
    return this._left.asObservable();
  }

  get up() {
    return this._up.asObservable();
  }

  get right() {
    return this._right.asObservable();
  }

  get down() {
    return this._down.asObservable();
  }

  protected _a = new Subject<void>();

  protected _b = new Subject<void>();

  protected _left = new Subject<void>();

  protected _up = new Subject<void>();

  protected _right = new Subject<void>();

  protected _down = new Subject<void>();

}
