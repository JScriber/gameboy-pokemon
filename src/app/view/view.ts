import Konva from 'konva';

// Groups.
import { TextboxGroup } from './groups/textbox/textbox-group';
import { SceneGroup, PokemonPosition } from './groups/scene/scene-group';
import { TeamGroup } from './groups/team/team-group';

// View assets.
import { FieldStyle } from './assets/field-style';
import { PlatformStyle } from './assets/platform-style';
import { TextboxStyle } from './assets/textbox-style';

// Models.
import { Pokemon } from '../models/Pokemon';
import { Attack } from '../models/Attack';
import { ImageBank, pkmnBackImageID, pkmnFrontImageID, pkmnIconImageID } from './utils/image-bank';

// Inputs.
import { InputAggregator } from './input/input-aggregator';
import { Observable, timer, of } from 'rxjs';
import { Team } from '../models/Trainer';

export enum Action {
  ATTACK,
  TEAM,
  BAG,
  RUN
}

/** Graphical view options. */
interface GraphicStyle {
  field: FieldStyle;
  platform: PlatformStyle;
  textbox: TextboxStyle;
}

/**
 * Application view.
 */
export class View {

  /** Width of the view. */
  public static readonly WIDTH = 296;

  /** Height of the view. */
  public static readonly HEIGHT = 215;

  /** ID of the node where the view is mounted. */
  private static readonly MOUNT_POINT = 'container';

  /** Base canvas. */
  private readonly stage = new Konva.Stage({
    container: View.MOUNT_POINT,
    width: View.WIDTH,
    height: View.HEIGHT
  });

  /** Unique layer. */
  private readonly layer = new Konva.Layer();

  /** Scene representation. */
  private readonly sceneGroup = new SceneGroup({
    x: 0,
    y: 0,
    width: SceneGroup.WIDTH,
    height: SceneGroup.HEIGHT
  });

  /** Team representation. */
  private readonly teamGroup = new TeamGroup({
    x: 0,
    y: 0,
    width: View.WIDTH,
    height: View.HEIGHT
  });
  
  /** Textbox representation. */
  private readonly textboxGroup = new TextboxGroup({
    x: 0,
    y: SceneGroup.HEIGHT,
    width: this.stage.width(),
    height: TextboxGroup.HEIGHT
  });

  constructor(input: InputAggregator) {    
    this.stage.add(this.layer);

    this.layer.add(this.sceneGroup);
    this.layer.add(this.textboxGroup);
    this.layer.add(this.teamGroup);

    // Propagate the input.
    this.textboxGroup.registerInput(input);
  }

  /**
   * Initializes the view.
   * @param graphicStyle
   * @param foregroundTeam
   * @param backgroundTeam
   */
  async init({ field, platform, textbox }: GraphicStyle, foregroundTeam: Team, backgroundTeam: Team) {

    // Load the global assets.
    await ImageBank.instance.load([
      {
        id: 'male',
        source: 'gender/male.png'
      },
      {
        id: 'female',
        source: 'gender/female.png'
      },
      {
        id: 'cursor',
        source: 'textboxes/secondary/cursor.png'
      },
      {
        id: 'team-background',
        source: 'team/background.jpg'
      },
      {
        id: 'team-pokeball',
        source: 'team/pokeball.png'
      },
      {
        id: 'team-pokeball-selected',
        source: 'team/pokeball-selected.png'
      },
      {
        id: 'team-first-pokemon',
        source: 'team/first-pokemon.png'
      },
      {
        id: 'team-first-pokemon-selected',
        source: 'team/first-pokemon-selected.png'
      }
    ]);

    // Loads the foreground pokemon images.
    await ImageBank.instance.load(foregroundTeam.map(({ graphicID }) => ({
      id: pkmnBackImageID(graphicID),
      source: `pokemons/back/${graphicID}.png`
    })));

    // Loads the background pokemon images.
    await ImageBank.instance.load(backgroundTeam.map(({ graphicID }) => ({
      id: pkmnFrontImageID(graphicID),
      source: `pokemons/front/${graphicID}.gif`
    })));

    // Loads the pokemon icons.
    await ImageBank.instance.load(foregroundTeam.map(({ graphicID }) => ({
      id: pkmnIconImageID(graphicID),
      source: `pokemons/icons/${graphicID}.png`
    })));


    // Load the scene.
    await this.sceneGroup.load(field, platform);
    
    // Load the textbox.
    await this.textboxGroup.load(textbox);

    // Load the team.
    //this.teamGroup.load();

    this.layer.batchDraw();
  }

  /**
   * Displays the pokemon at the given position.
   * @param pokemon
   * @param position
   */
  displayPokemon(pokemon: Pokemon, position: PokemonPosition) {
    this.sceneGroup.displayPokemon(pokemon, position);
    this.layer.batchDraw();
  }

  /**
   * Displays the action menu on the screen.
   * @param pokemon
   */
  showActionMenu(pokemon: Pokemon): Observable<Action> {
    this.textboxGroup.displayMessage(`Que doit faire ${pokemon.name} ?`);

    return this.textboxGroup.showActionMenu();
  }

  /**
   * Displays the attack menu on the screen.
   * @param pokemon
   */
  showAttackMenu(pokemon: Pokemon): Observable<Attack> {
    return this.textboxGroup.showAttackMenu(pokemon.attacks);
  }

  /**
   * Displays the team menu on the screen.
   * @param pokemons 
   */
  showTeamMenu(pokemons: Pokemon[]): Observable<Pokemon> {

    this.teamGroup.display(pokemons);

    return of(pokemons[0]);
  }

  /**
   * Displays the message on screen.
   * @param message
   * @param timeout
   */
  displayMessage(message: string, timeout = 1000): Observable<number> {
    this.textboxGroup.displayMessage(message);

    return timer(timeout);
  }
}
