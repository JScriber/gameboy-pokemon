import { View, Action } from './view/view';

// View assets.
import { FieldStyle } from './view/assets/field-style';
import { PlatformStyle } from './view/assets/platform-style';
import { TextboxStyle } from './view/assets/textbox-style';

// Builders.
import { PokemonBuilder } from './builders/PokemonBuilder';
import { AttackBuilder } from './builders/AttackBuilder';
import { PokemonPosition } from './view/groups/scene/scene-group';
import { Gender } from './models/gender';

// Inputs.
import { InputAggregator } from './view/input/input-aggregator';
import { KeyboardInput } from './view/input/input-methods/keyboard-input';
import { GameboyInput } from './view/input/input-methods/gameboy-input';
import { GamepadInput } from './view/input/input-methods/gamepad-input';
import { MoveType } from './models/MoveType';
import { Type } from './models/Type';
import { Team } from './models/Trainer';

function fakeData(): [Team, Team] {

  const builder = PokemonBuilder.instance;

  const atkBuilder = AttackBuilder.instance;

  // Some view actions.
  const latios = builder.build({
    id: 191,
    name: 'Latios',
    gender: Gender.FEMALE,

    skill: null,
    level: 75,
    types: [],
    attacks: [
    ],

    hp: 385,
    attack: 100,
    specialAttack: 100,
    shield: 100,
    specialShield: 100,
    speed: 200,

    graphicID: 381
  });

  const raichu = builder.build({
    id: 26,
    name: 'Raichu',
    gender: Gender.MALE,

    skill: null,
    level: 24,
    types: [],
    attacks: [],
    hp: 120,
    attack: 100,
    specialAttack: 100,
    shield: 100,
    specialShield: 100,
    speed: 200,

    graphicID: 26
  });

  const alakazam = builder.build({
    id: 138,
    name: 'Alakazam',
    gender: Gender.MALE,

    skill: null,
    level: 10,
    types: [],
    attacks: [

      atkBuilder.build({
        name: 'Charge',
        moveType: MoveType.PHYSICAL,
        power: 50,
        pp: 15,
        type: Type.NORMAL
      }),
      atkBuilder.build({
        name: 'Draco meteor',
        moveType: MoveType.PHYSICAL,
        power: 50,
        pp: 5,
        type: Type.DRAGON
      }),
      atkBuilder.build({
        name: 'Focus energy',
        moveType: MoveType.SPECIAL,
        power: 120,
        pp: 30,
        type: Type.PSYCHIC
      }),
      atkBuilder.build({
        name: 'Thunder wave',
        moveType: MoveType.PHYSICAL,
        power: 50,
        pp: 10,
        type: Type.ELECTRIC
      })
    ],

    hp: 300,
    attack: 100,
    specialAttack: 100,
    shield: 100,
    specialShield: 100,
    speed: 200,

    graphicID: 65
  });

  return [
    [ alakazam ],
    [ latios, raichu ]
  ];
}


window.addEventListener('load', async () => {
  
  // Input initialization.
  const input = new InputAggregator([
    new KeyboardInput(),
    new GameboyInput(),
    new GamepadInput()
  ]);

  // View initialization.
  const view = new View(input);

  const [team1, team2] = fakeData();

  // Initialize the scene.
  await view.init({
    field: FieldStyle.FOREST,
    platform: PlatformStyle.GROUND,
    textbox: TextboxStyle.SQUARED
  }, team1, team2);

  
  view.displayPokemon(team1[0], PokemonPosition.FOREGROUND);

  view.displayPokemon(team2[0], PokemonPosition.BACKGROUND);

  // view.showTeamMenu(team1);

  while (1) {
    const action = await view.showActionMenu(team1[0]).toPromise();
  
    if (action === Action.ATTACK) {
      const attack = await view.showAttackMenu(team1[0]).toPromise();

      attack.pp.current --;

      await view.displayMessage(`${team1[0].name} lance ${attack.name}.`).toPromise();

      team2[0].hp.current -= 80;
      view.displayPokemon(team2[1], PokemonPosition.BACKGROUND);
    } else {
      await view.displayMessage('Action non implémentée.').toPromise();
    }
  }
});
